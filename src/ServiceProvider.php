<?php
namespace KDA\Laravel\Invoice;

use KDA\Laravel\Invoice\Facades\Invoice;
use KDA\Laravel\Invoice\Invoice as InvoiceInvoice;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasConfig;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use HasConfig;

    protected $configs = [
        'kda/invoice.php'=>'kda.invoice'
    ];
    protected $packageName ='laravel-invoice';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        
        $this->app->singleton(Invoice::class, function () {
            return new InvoiceInvoice();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
