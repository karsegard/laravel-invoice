<?php

namespace KDA\Laravel\Invoice\Models\Concerns;


trait HasTax
{

    public static function bootHasTax(): void
    {
        
    }
    public function initializeHasTax(): void
    {
        $this->fillable[]='taxable_id';
        $this->fillable[]='taxable_type';
    }

    public function taxable()
    {
        return $this->morphTo();
    }


}
