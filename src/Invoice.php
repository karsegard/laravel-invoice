<?php
namespace KDA\Laravel\Invoice;

use Closure;
use Illuminate\Database\Eloquent\Model;

//use Illuminate\Support\Facades\Blade;
class Invoice 
{

    protected Closure $getTaxUsing;


    public function evaluate($value, array $parameters = [])
    {
        if ($value instanceof Closure) {
            return app()->call(
                $value,
                $parameters
            );
        }

        return $value;
    }

    public function getTaxUsing(Closure $closure){
        $this->getTaxUsing = $closure;
    }
    
    public function getTax(?Model $model){
        return $this->evaluate($this->getTaxUsing,['model'=>$model]);
    }
}