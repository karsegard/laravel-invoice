<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Invoice\Models\Invoice;
use KDA\Laravel\Invoice\Models\InvoiceItem;
use KDA\Laravel\Invoice\Models\InvoiceItemUnit;

use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function create_invoice()
  {
    $o = Invoice::factory()->create([]);
    $this->assertNotNull($o);
    $this->assertNotNull($o->reference);
  }

  /** @test */
  function create_invoice_item()
  {
    $o = InvoiceItem::factory()->create([]);
    $this->assertNotNull($o);
    $this->assertEquals($o->total_net, $o->qty * $o->unit_price);
    $this->assertEquals($o->total_net,$o->invoice->amount_net);
    $this->assertNotNull($o->unit);

  }

  /** @test */
  function create_invoice_item_unit()
  {
      $o = InvoiceItemUnit::factory()->create([]);
      $this->assertNotNull($o);
  }


}
