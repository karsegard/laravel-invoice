<?php
namespace KDA\Laravel\Invoice\Models\Contracts;

interface Taxable {
    public function getTaxRate(Model $model):float;
}
