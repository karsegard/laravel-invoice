<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('invoice_id');
            $table->nullableNumericMorphs('payable');
            $table->string('label');
            $table->foreignId('unit_id');
            $table->decimal('unit_price');
            $table->decimal('qty');
            $table->decimal('total_net')->nullable();
            $table->decimal('total_ttc')->nullable();
            $table->nullableNumericMorphs('taxable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
};
