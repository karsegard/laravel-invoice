<?php

namespace KDA\Laravel\Invoice\Models\Concerns;

use KDA\Laravel\Invoice\Models\InvoiceItem;

trait LinesRelation
{

    public static function bootLinesRelation(): void
    {
        
    }
    public function initializeLinesRelation(): void
    {
    }

 
    public function lines()
    {
        return $this->hasMany(config('kda.invoice.class.item',InvoiceItem::class));
    }


}
