<?php

namespace KDA\Laravel\Invoice\Models\Concerns;


trait UpdateInvoiceAmount
{

    public static function bootUpdateInvoiceAmount(): void
    {
        static::updated(function($model){
            if($model->invoice){
                $model->invoice->recompute();
            }   
        }); 
        static::created(function($model){
            if($model->invoice){
                $model->invoice->recompute();
            }   
        });
       
    }

    public function initializeUpdateInvoiceAmount(): void
    {
       
    }



}
