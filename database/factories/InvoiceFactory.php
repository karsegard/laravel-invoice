<?php

namespace KDA\Laravel\Invoice\Database\Factories;

use KDA\Laravel\Invoice\Models\Invoice;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    protected $model = Invoice::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
