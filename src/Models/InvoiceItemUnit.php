<?php

namespace KDA\Laravel\Invoice\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Invoice\Database\Factories\InvoiceItemUnitFactory;

/**
 * @mixin IdeHelperInvoiceItemUnit
 */
class InvoiceItemUnit extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'short',
    ];
    protected static function newFactory()
    {
        return  InvoiceItemUnitFactory::new();
    }
}
