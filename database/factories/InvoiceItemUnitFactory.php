<?php

namespace KDA\Laravel\Invoice\Database\Factories;

use KDA\Laravel\Invoice\Models\InvoiceItemUnit;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceItemUnitFactory extends Factory
{
    protected $model = InvoiceItemUnit::class;

    public function definition()
    {
        return [
            //
            'name'=>$this->faker->randomElement(['Hour','Item','Platypus']),
            'short'=>$this->faker->randomElement(['h','u','pl']),
        ];
    }
}
