<?php

namespace KDA\Laravel\Invoice\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;
use KDA\Laravel\Invoice\Models\Concerns\BillableRelation;
use KDA\Laravel\Invoice\Models\Concerns\AutoIncrementReference;
use KDA\Laravel\Invoice\Models\Concerns\HasTax;
use KDA\Laravel\Invoice\Models\Concerns\LinesRelation;

/**
 * @mixin IdeHelperInvoice
 */
class Invoice extends Model
{
    use HasFactory;
    use HasDefaultAttributes;
    use BillableRelation;
    use AutoIncrementReference;
    use LinesRelation;
    use HasTax;

    protected $fillable = [
        'date',
        'reference',
        'amount_net',
        'amount_ttc',
    ];

    protected static function newFactory()
    {
        return  \KDA\Laravel\Invoice\Database\Factories\InvoiceFactory::new();
    }


    public function createDefaultAttributes(){
        $this->defaultAttribute('date',Carbon::now());
        $this->defaultAttribute('reference',$this->getNextReference());
    }

    public function recompute(){
        $amount = $this->fresh()->lines->reduce(function($amount,$line){
            $amount+=$line->total_net;
            return $amount;
        },0);
        $this->amount_net = $amount;
        $this->save();
    }
   
    
}
