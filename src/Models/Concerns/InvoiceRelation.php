<?php

namespace KDA\Laravel\Invoice\Models\Concerns;

use KDA\Laravel\Invoice\Models\Invoice;

trait InvoiceRelation
{

    public static function bootInvoiceRelation(): void
    {
        
    }
    public function initializeInvoiceRelation(): void
    {
    }

 
    public function invoice()
    {
        return $this->belongsTo(config('kda.invoice.class.invoice',Invoice::class));
    }

}
