<?php

namespace KDA\Laravel\Invoice\Database\Factories;

use KDA\Laravel\Invoice\Models\InvoiceItem;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Invoice\Models\Invoice;
use KDA\Laravel\Invoice\Models\InvoiceItemUnit;

class InvoiceItemFactory extends Factory
{
    protected $model = InvoiceItem::class;

    public function definition()
    {
       
        return [
            //
            'invoice_id'=>Invoice::factory(),
            'label'=>$this->faker->word(),
            'unit_id'=>InvoiceItemUnit::factory(),
            'unit_price'=>$this->faker->numberBetween(10,1400),
            'qty'=>$this->faker->numberBetween(1,20),
        ];
    }
}
