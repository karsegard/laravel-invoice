<?php

namespace KDA\Laravel\Invoice\Models\Concerns;

use Carbon\Carbon;
use Closure;
use DB;
trait AutoIncrementReference
{

    protected function nextAutoIncrementId(){
        $statement = DB::select("SHOW TABLE STATUS LIKE '".$this->getTable()."'");
        return  $statement[0]->Auto_increment;
    }

    public function getNextReference()
    {
        $digits = $this->reference_digits ?? 4;
        $prefix = Carbon::now()->year;

        $s = sprintf('%0'.$digits.'d', static::whereYear('date',$prefix)->count()+1);
        return $prefix."-".$s;
    }
}
