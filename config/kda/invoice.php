<?php
use KDA\Laravel\Invoice\Models\InvoiceItem;
use KDA\Laravel\Invoice\Models\Invoice;
use KDA\Laravel\Invoice\Models\InvoiceItemUnit;

return [

    'class'=>[
        'item'=>InvoiceItem::class,
        'unit'=>InvoiceItemUnit::class,
        'invoice'=>Invoice::class,
    ]
];