<?php

namespace KDA\Laravel\Invoice\Models\Concerns;


trait Payable
{

    public static function bootPayable(): void
    {
        
    }
    public function initializePayable(): void
    {
        $this->fillable[]='payable_type';
        $this->fillable[]='payable_id';

    }



}
