<?php

namespace KDA\Laravel\Invoice\Models\Concerns;


trait BillableRelation
{

    public static function bootBillableRelation(): void
    {
        
    }
    public function initializeBillableRelation(): void
    {
        $this->fillable[]='billable_id';
        $this->fillable[]='billable_type';
    }

    public function billable()
    {
        return $this->morphTo();
    }


}
