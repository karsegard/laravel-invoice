<?php

namespace KDA\Laravel\Invoice\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;
use KDA\Laravel\Invoice\Database\Factories\InvoiceItemFactory;
use KDA\Laravel\Invoice\Models\Concerns\HasTax;
use KDA\Laravel\Invoice\Models\Concerns\InvoiceRelation;
use KDA\Laravel\Invoice\Models\Concerns\Payable;
use KDA\Laravel\Invoice\Models\Concerns\UnitRelation;
use KDA\Laravel\Invoice\Models\Concerns\UpdateInvoiceAmount;

/**
 * @mixin IdeHelperInvoiceItem
 */
class InvoiceItem extends Model
{
    use HasFactory;
    use HasDefaultAttributes;
    use Payable;
    use InvoiceRelation;
    use UpdateInvoiceAmount;
    use UnitRelation;
    use HasTax;

    protected $fillable = [
        'invoice_id',
        'label',
        'unit_id',
        'unit_price',
        'qty',
        'total_net',
        'total_ttc',
    ];

    protected static function newFactory()
    {
        return InvoiceItemFactory::new();
    }
  
    public function applyDefaultAttributes()
    {
        $this->defaultAttribute('total_net',$this->unit_price * $this->qty);
    }

  

    
}
