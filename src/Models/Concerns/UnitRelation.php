<?php

namespace KDA\Laravel\Invoice\Models\Concerns;

use KDA\Laravel\Invoice\Models\InvoiceItem;

trait UnitRelation
{

    public static function bootUnitRelation(): void
    {
        
    }
    public function initializeUnitRelation(): void
    {
    }

 

    public function unit()
    {
        return $this->belongsTo(config('kda.invoice.class.unit',InvoiceItemUnit::class), 'unit_id');
    }
}
